
/********************************************************
 *   PL - The driver of a ring network on base UART   *
 *                                                       *
 *     author Tumanov Stanislav Aleksandrovich,          *
 *     Russia, Kirov city, maintumanov@mail.ru           *
 *     #define PLPort - ???? RS232
 *********************************************************/
//#include "PL.h"


#ifndef PLReciveBufferSize
  #define PLReciveBufferSize 44
#endif

#ifndef PLUARTspeed
  #define PLUARTspeed 115200
#endif

#ifndef PLDataBufferSize
  #define PLDataBufferSize 16
#endif

#ifndef PLDefault_Mode
  #define PLDefault_Mode 0
#endif

#ifndef PLDefaultAddress
  #define PLDefaultAddress 2
#endif

#ifndef PLPort
  #define PLPort PLstream
#endif

typedef void(*PLPOnReadData)(byte, byte);


int1 PLavailable(void);
void PLInit(int16 patternIndex);
void PLSendByte(byte Data);
void PLAnalysis(byte data);
void PLCheck(void);
void PLSendStart(void);
void PLSetNetSpeed(byte Speed);
byte PLRead(void);
void PLMesAnalys();
void PLSendInt16(int16 Data);
void PLSendData(byte channel, byte DSize, byte *Data);
void PLDataSend(byte DSize, byte *Data);
void PLSendInt8(byte channel, byte Data);
void PLSendInt16(byte channel, int16 Data);
byte PLGetInt8();
int16 PLGetInt16();


//-----------------------------------------------------------------//
//            VARIABLES
//-----------------------------------------------------------------//


PLPOnReadData PLOnReadData;
int1 PLPSendEnable = 1;

//BUFFER VARIABLES
byte PLReadBuffer[PLReciveBufferSize];
byte PLBuffTail = 0;
byte PLBuffHead = 0;
byte PLBuffCount = 0;


//MESSAGE VARIABLES
byte PLPDataBuff[PLDataBufferSize];
byte PLPAnalysisState = 0;
byte PLPDataPos = 0;
byte PLPDataSize;
byte PLPDataChannel;
int16 PLPPatternIndex = 0;


//-----------------------------------------------------------------//
//            FUNCTIONS                                            //
//-----------------------------------------------------------------//

void PLInit(int16 patternIndex)
{
  set_uart_speed(PLUARTspeed);
  enable_interrupts(INT_RDA);
  PLPPatternIndex = patternIndex;
//  enable_interrupts(GLOBAL);
}

//-------------------------------------------------------------------------

#int_RDA
void PLRDA_isr(void)
{
  do
  {
    PLReadBuffer[PLBuffTail] = fgetc(PLPort);
    PLBuffCount++;
    if (PLBuffTail++ == PLReciveBufferSize - 1)
      PLBuffTail = 0;
  }
  while (kbhit());
}

//-------------------------------------------------------------------------

void PLCheck(void)
{
  while (PLBuffCount > 0)
    PLAnalysis(PLRead());
}

//-------------------------------------------------------------------------

byte PLRead(void)
{
  byte res;
  res = PLReadBuffer[PLBuffHead];
  PLBuffCount--;
  if (PLBuffHead++ == PLReciveBufferSize - 1)
    PLBuffHead = 0;
  return (res);
}

//-------------------------------------------------------------------------

void PLAnalysis(byte data)
{
  if (data == 253)
  {
    PLPAnalysisState = 1;
    return ;
  }

  if (data == 255)
    PLPAnalysisState += 50;
  else
  if (PLPAnalysisState >= 50)
  {
    PLPAnalysisState -= 50;
    data += 128;
  };
  
  switch (PLPAnalysisState)
  {
    case 1:
      PLPDataChannel = data;
      PLPDataPos = 0;
      PLPAnalysisState = 2;
      break;
    case 2:
      PLPDataSize = data;
      PLPDataPos = 0;
      PLPAnalysisState = 3;
      break;
    case 3:
      if (PLDataBufferSize > PLPDataPos) PLPDataBuff[PLPDataPos] = data;
      PLPDataPos++;
      if (PLPDataPos == PLPDataSize) {
        PLPAnalysisState = 0;
        if (PLPDataChannel == 255) {
            if (PLPDataBuff[0] == 255) PLPSendEnable = 1;
            if (PLPDataBuff[0] == 0) PLPSendEnable = 0;
        } else (*PLOnReadData)(PLPDataChannel, PLPDataBuff[0]);
      }
      break;
  }
}

//-------------------------------------------------------------------------

void PLSendStart(void)
{
  fputc(253, PLPort);
  PLSendInt16(PLPPatternIndex);
}

//-------------------------------------------------------------------------

void PLSendByte(byte Data)
{
  if (Data < 253)
    fputc(Data, PLPort);
  else
  {
    fputc(255, PLPort);
    fputc(Data - 128, PLPort);
  }
}

//-------------------------------------------------------------------------

void PLSendInt16(int16 Data)
{
  PLSendByte(Data >> 8);
  PLSendByte(Data &0xFF);
}

//-------------------------------------------------------------------------

void PLDataSend(byte DSize, byte *Data)
{
  byte i = 0;
  PLSendByte(DSize);
  for (i = 0; i < DSize; i++)
    PLSendByte(Data[i]);
}


//-------------------------------------------------------------------------???????? ??????

void PLSendData(byte channel, byte DSize, byte *Data)
{
  PLSendStart();
  PLSendByte(channel);
  PLDataSend(DSize, Data);
}

void PLSendInt8(byte channel, byte Data)
{
   if(!PLPSendEnable) return;
   PLSendStart();
   PLSendByte(channel);
   PLSendByte(2);
   PLSendByte(5);
   PLSendByte(Data);
}

void PLSendInt16(byte channel, int16 Data)
{
   if(!PLPSendEnable) return;
   PLSendStart();
   PLSendByte(channel);
   PLSendByte(3);
   PLSendByte(8);
   PLSendInt16(Data);
}

byte PLGetInt8()
{
  return PLPDataBuff[1];
}

int16 PLGetInt16()
{
  return make16(PLPDataBuff[1], PLPDataBuff[2]);
}

