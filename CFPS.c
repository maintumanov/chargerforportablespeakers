#include <CFPS.h>
#include "PL.c"

//ADC VARIABLES
signed int16 batteryVoltage = 0;
signed int16 inputVoltage = 0;
signed int16 diffVoltage = 0;
unsigned int8 readChannel = 0;

//RELAY VARIABLES
unsigned int8 relayState = 0;
int1 batteryIndicatorState = 0;

//CHARGE VARIABLES
unsigned int8 chargeState = 0;
unsigned int16 chargeTimer = 0;
int1 chargeIndicatorState = 0;

//TASK VOLTAGE READING PROCESSED
#task(rate = 100 ms, max = 200 us)
void task_read_voltages();

//TASK RELAY PROCESSED
#task(rate = 1000 ms, max = 200 us)
void task_control_relay();

//TASK CHARGE PROCESSED
#task(rate = 500 ms, max = 200 us)
void task_control_charge();
int16 calcPWMvol();

//============================================================
void task_read_voltages() {
   switch (readChannel) {
      case 0: 
         batteryVoltage = read_adc(ADC_READ_ONLY);
        // PLSendInt16(0, batteryVoltage);
         readChannel = 1;
         break;
      case 1: 
         inputVoltage = read_adc(ADC_READ_ONLY);
        // PLSendInt16(1, inputVoltage);
         readChannel = 0;
         break;         
   }
   set_adc_channel(readChannel);
   read_adc(ADC_START_ONLY);
   diffVoltage = inputVoltage - batteryVoltage;
 //  PLSendInt16(3, diffVoltage);
}

void task_control_relay() {
   
   if (inputVoltage < 440 && batteryVoltage > 421) relayState = 0;
   
   switch(relayState) {
         case 0: //indicator set, wait input and battery
            output_low(RED2);
            set_pwm2_duty((int16)0);
            if (batteryVoltage <= 421) {
               if (batteryIndicatorState) output_high(YELLOW);
               else output_low(YELLOW); 
            }
            relayState = 1;
            break;
         case 1:   
            output_high(RED2);
            output_low(YELLOW);
            set_pwm2_duty((int16)4035);
            relayState = 2;
            break;
         case 2:
            set_pwm2_duty((int16)2000); 
            break;
   }
}


void task_control_charge() {
      
      if (diffVoltage < (int16)55 || batteryVoltage < 385) chargeState = 0; //518
   
      switch(chargeState) {
         case 0: //indicator set, wait input and battery
            output_low(GREEN);
            if (inputVoltage > 440) {
               if (chargeIndicatorState) output_high(RED);
               else output_low(RED);
               chargeIndicatorState = !chargeIndicatorState;
            } else output_low(RED);
            chargeState = 1;
            break;
         case 1: //charge timer set
            output_high(RED);
            chargeTimer = 40;
            chargeState = 2;
            break;
         case 2: //charge
            //set_pwm1_duty((int16)80); //todo correct
            set_pwm1_duty(calcPWMvol());
            if (chargeTimer > 0) chargeTimer --;
            if (chargeTimer == 0) chargeState = 3;
            break;
         case 3: //charge stop
            set_pwm1_duty((int16)0);
            chargeState = 4;
            break;
         case 4: //test battery voltge
            if (batteryVoltage < 530) chargeState = 1;
            else chargeState = 5;
            break;
         case 5: //indicator set
            output_high(GREEN);
            output_low(RED);
            chargeState = 6;
            break;
         case 6: //test battery voltge   
            if (batteryVoltage < 479) chargeState = 0;
            break;
      }
}

int16 calcPWMvol() {
   if (diffVoltage < 38) return (int16)0;
   if (diffVoltage < 76) return (int16)100;
   if (diffVoltage < 95) return (int16)70;
   if (diffVoltage < 114) return (int16)45;
   if (diffVoltage < 133) return (int16)30;
   if (diffVoltage < 152) return (int16)25;
   if (diffVoltage < 171) return (int16)20;
   if (diffVoltage < 190) return (int16)15;
   if (diffVoltage < 209) return (int16)12;
   if (diffVoltage < 228) return (int16)10;
   if (diffVoltage < 247) return (int16)8;
   if (diffVoltage < 266) return (int16)7;
   if (diffVoltage < 304) return (int16)6;
   if (diffVoltage < 342) return (int16)5;
   if (diffVoltage < 380) return (int16)4; 
   return (int16)2; 
}

void initialization() {
   SET_TRIS_A( 0x03 );
   SET_TRIS_B( 0x00 );
   SET_TRIS_C( 0x80 );
   setup_adc_ports(AN0_TO_AN1);
   setup_adc(ADC_CLOCK_INTERNAL|ADC_TAD_MUL_0);
  // setup_timer_2(T2_DIV_BY_16,255,1);      //0,2 us overflow, 0,2 us interrupt
   setup_timer_2(T2_DIV_BY_1,255,1);

   setup_ccp1(CCP_PWM);
   setup_ccp2(CCP_PWM);
   set_pwm1_duty((int16)0);
   set_pwm2_duty((int16)0);
    
   set_adc_channel(readChannel);
   read_adc(ADC_START_ONLY);

   output_low(RED);
   output_low(RED2);
   output_low(GREEN);
   output_low(YELLOW);
   PLInit(10);
}

void main()
{
   initialization();
   rtos_run();
}
