#include <18F2520.h>
#device ADC = 10
#device *=16

#FUSES PUT                      //Power Up Timer
#FUSES NOBROWNOUT               //No brownout reset
#FUSES BORV27                   //Brownout reset at 2.7V
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOXINST                  //Extended set extension and Indexed Addressing mode disabled (Legacy mode)

#use delay(crystal=20000000)
#use rtos(timer=0, minor_cycle=1 ms)
#use rs232(baud=9600, parity=N, xmit=PIN_C6, rcv=PIN_C7, bits=8, stream = PLstream, ERRORS)

#define RED  PIN_B1
#define GREEN  PIN_B2
#define YELLOW  PIN_B3
#define RED2  PIN_B4
